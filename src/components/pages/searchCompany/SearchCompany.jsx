import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Information from './Information';
import CompanyInformation from './CompanyInformation';
import { GetCompanyByIdentifierAction } from '../../../actions/GetCompanyByIdentifierAction';
import { identifierIsValid } from '../../../helpers/validators/IdentifierValidator';

class SearchCompany extends Component {
  state = {
    identifier: '',
    showCompanyInformation: false,
    identifierError: ''
  };

  onSubmit = e => {
    e.preventDefault();

    const identifier = this.state.identifier;

    if (!identifierIsValid(identifier)) {
      this.setState({
        identifier: '',
        showCompanyInformation: false,
        identifierError: 'Wpisany identyfikator ma niepoprawny format'
      });

      return false;
    }

    this.props.getCompanyByIdentifier(identifier);

    this.setState({
      identifier: '',
      showCompanyInformation: true,
      identifierError: ''
    });
  };

  onInputChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <React.Fragment>
        <Information />

        <div className="col-md-8 mx-auto" onSubmit={this.onSubmit}>
          <form>
            <label htmlFor="identifier">NIP/KRS/REGON</label>

            <div className="input-group">
              <input
                type="text"
                className="form-control"
                name="identifier"
                value={this.state.identifier}
                onChange={this.onInputChange}
              />

              <div className="input-group-append">
                <button type="submit" className="btn btn-primary">
                  Pobierz dane
                </button>
              </div>
            </div>

            <div
              className={
                this.state.identifierError.length > 0
                  ? 'invalid-feedback d-block'
                  : 'invalid-feedback'
              }
            >
              {this.state.identifierError}
            </div>
          </form>
        </div>

        {this.state.showCompanyInformation && <CompanyInformation />}
      </React.Fragment>
    );
  }
}

// PropTypes
SearchCompany.propTypes = {
  getCompanyByIdentifier: PropTypes.func.isRequired
};

export default connect(
  null,
  { getCompanyByIdentifier: GetCompanyByIdentifierAction }
)(SearchCompany);

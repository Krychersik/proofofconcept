import React, { Component } from 'react';
import Header from './Header';
import SearchCompany from './SearchCompany';

class SearchCompanyPage extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <SearchCompany />
      </React.Fragment>
    );
  }
}

export default SearchCompanyPage;

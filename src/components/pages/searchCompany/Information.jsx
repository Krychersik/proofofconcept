import React, { Component } from 'react';

class Information extends Component {
  render() {
    return (
      <div className="col-md-8 mx-auto">
        <div className="py-5 text-justify">
          <h4>
            <span className="fa fa-info mr-2"></span>
            Informacja
          </h4>

          <p className="lead">
            Przykładowy nr NIP pobierający poprawne dane to 777-777-77-77, można
            go wpisać w dowolnym formacie (np. format europejski PL7777777777
            lub format bez myślników 7777777777).
          </p>
          <p className="lead">
            Przykładowy nr KRS pobierający poprawne dane to 0000133156.
          </p>
          <p className="lead">
            Przykładowy nr REGON pobierający poprawne dane to 331438036.
          </p>
        </div>
      </div>
    );
  }
}

export default Information;

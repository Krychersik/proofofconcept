import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ErrorAlert from '../alerts/ErrorAlert';

class CompanyInformation extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="col-md-8 mx-auto">
          <div>
            <hr className="mb-4 mt-4" />

            <div className="mb-3">
              <label>Nazwa firmy</label>
              <label type="text" className="form-control">
                {this.props.company.name}
              </label>
            </div>

            <div className="row">
              <div className="col-md-9 mb-3">
                <label>Ulica</label>
                <label type="text" className="form-control">
                  {this.props.company.address &&
                    this.props.company.address.street}
                </label>
              </div>

              <div className="col-md-3 mb-3">
                <label>Numer</label>
                <label type="text" className="form-control">
                  {this.props.company.address &&
                    this.props.company.address.number}
                </label>
              </div>
            </div>

            <div className="row">
              <div className="col-md-4 mb-3">
                <label>Kod pocztowy</label>
                <label type="text" className="form-control">
                  {this.props.company.address &&
                    this.props.company.address.postalCode}
                </label>
              </div>

              <div className="col-md-4 mb-3">
                <label>Miasto</label>
                <label type="text" className="form-control">
                  {this.props.company.address &&
                    this.props.company.address.city}
                </label>
              </div>

              <div className="col-md-4 mb-3">
                <label>Kraj</label>
                <label type="text" className="form-control">
                  {this.props.company.address &&
                    this.props.company.address.country}
                </label>
              </div>
            </div>

            <hr className="mb-4" />
          </div>
        </div>

        {this.props.errors.length > 0 && (
          <ErrorAlert errorMessage={'Nie znaleziono danych firmy!'} />
        )}
      </React.Fragment>
    );
  }
}

// PropTypes
CompanyInformation.propTypes = {
  company: PropTypes.object.isRequired,
  errors: PropTypes.array
};

const mapStateToProps = state => ({
  company: state.companyReducer.company,
  errors: state.errorsReducer.errors
});

export default connect(
  mapStateToProps,
  {}
)(CompanyInformation);

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ErrorAlert extends Component {
  render() {
    const { errorMessage } = this.props;

    return (
      <div className="col-md-8 mx-auto mt-2 mb-2">
        <div className="alert alert-danger" role="alert">
          {errorMessage}
        </div>
      </div>
    );
  }
}

// PropTypes
ErrorAlert.propTypes = {
  errorMessage: PropTypes.string.isRequired
};

export default ErrorAlert;

import React, { Component } from 'react';
import './App.css';
import SearchCompanyPage from './components/pages/searchCompany/SearchCompanyPage';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <main className="container">
          <SearchCompanyPage />
        </main>
      </React.Fragment>
    );
  }
}

export default App;

import axios from 'axios';
import { GET_COMPANY_BY_IDENTIFIER, ERROR, UNKNOWN_ERROR } from './Types';

export const GetCompanyByIdentifierAction = identifier => dispatch => {
  const params = {
    identifier: identifier
  };

  axios
    .get('https://localhost:5001/api/v1/company', { params })
    .then(response => {
      dispatch({
        type: GET_COMPANY_BY_IDENTIFIER,
        payload: response.data.data
      });
    })
    .catch(error => {
      if (error.response && error.response.data) {
        dispatch({
          type: ERROR,
          payload: error.response.data.errors
            ? error.response.data.errors
            : [
                {
                  title: error.response.data,
                  message: error.response.data
                }
              ]
        });
      } else {
        dispatch({
          type: UNKNOWN_ERROR,
          payload: [
            {
              title: 'Unexpected error',
              message: 'Unexpected error'
            }
          ]
        });
      }
    });
};

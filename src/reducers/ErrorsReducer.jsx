import { ERROR } from '../actions/Types';

const initialState = {
  errors: []
};

const ErrorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ERROR:
      return {
        ...state,
        errors: action.payload
      };

    default:
      return initialState;
  }
};

export default ErrorsReducer;

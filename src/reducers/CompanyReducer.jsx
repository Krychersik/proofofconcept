import { GET_COMPANY_BY_IDENTIFIER } from '../actions/Types';

const initialState = {
  company: {
    name: '',
    address: {
      street: '',
      number: '',
      postalCode: '',
      city: '',
      country: ''
    }
  }
};

const CompanyReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANY_BY_IDENTIFIER:
      return {
        ...state,
        company: action.payload
      };

    default:
      return initialState;
  }
};

export default CompanyReducer;

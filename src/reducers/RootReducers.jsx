import { combineReducers } from 'redux';
import CompanyReducer from './CompanyReducer';
import ErrorsReducer from './ErrorsReducer';

const RootReducers = combineReducers({
  companyReducer: CompanyReducer,
  errorsReducer: ErrorsReducer
});

export default RootReducers;

export function identifierIsValid(value) {
  if (
    isValidNipNumber(value) ||
    isValidRegonNumber(value) ||
    isValidKrsNumber(value)
  ) {
    return true;
  }

  return false;
}

function isValidNipNumber(value) {
  if (value.length === 0 || value.indexOf(' ') !== -1) {
    return false;
  }

  if (/([0-9]{3}-){2}[0-9]{2}-[0-9]{2}$/.test(value)) {
    value = value.replace(/-/g, '');
  } else if (/[A-Z]{2}[0-9]{10}$/.test(value)) {
    value = value.substring(2);
  } else if (!/[0-9]{10}$/.test(value)) {
    return false;
  }

  const divider = 11;
  const nipMultipliers = [6, 5, 7, 2, 3, 4, 5, 6, 7];
  var sum = getSum(value, nipMultipliers);
  var checksum = parseInt(value[value.length - 1]);
  var isValid = sum % divider === checksum;

  return isValid;
}

function isValidRegonNumber(value) {
  if (
    value.length === 0 ||
    value.indexOf(' ') !== -1 ||
    !/[0-9]{9}$/.test(value)
  ) {
    return false;
  }

  const divider = 11;
  const regonMultipliers = [8, 9, 2, 3, 4, 5, 6, 7];
  var sum = getSum(value, regonMultipliers);
  var checksum = parseInt(value[value.length - 1]);
  var isValid = sum % divider === checksum;

  return isValid;
}

function isValidKrsNumber(value) {
  if (
    value.length === 0 ||
    value.indexOf(' ') !== -1 ||
    !/[0-9]{10}$/.test(value)
  ) {
    return false;
  }

  return true;
}

function getSum(value, multipliers) {
  var sum = 0;

  for (var i = 0; i < value.length - 1; i++) {
    sum += multipliers[i] * value.charAt(i);
  }

  return sum;
}
